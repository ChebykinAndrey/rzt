#include "hardware_init.h"

#include "MDR32F9Qx_rst_clk.h"
#include "MDR32F9Qx_timer.h"

static unsigned int dddd;
void Frequency_CLK_PLL(unsigned pll_on, unsigned int pll_mul)
{
  // pll_mul - ����������� ��������� ��� CPU PLL
  // pll_pld - ��� ����������� PLL
  // pll_on - ��� ��������� PLL
  MDR_RST_CLK->HS_CONTROL = 0x01;                     // �������� ������� ��������� HSE (0b01)
  while (MDR_RST_CLK->CLOCK_STATUS == 0x00) __NOP();  // ������� ������� HSE � PLL (0b000)
  MDR_RST_CLK->CPU_CLOCK =  0x102;              			// �������� CPU_CLK (0b0100000010)

  if(pll_on == 1)
  {
    MDR_RST_CLK->PLL_CONTROL = (pll_on<<2)|(pll_mul<<8);	// ��� PLL
    dddd   = (pll_on<<2)|(pll_mul<<8);
    while (MDR_RST_CLK->CLOCK_STATUS < 0x06) __NOP();  	// ������� ������� HSE � PLL (0b110)

    MDR_RST_CLK->CPU_CLOCK = 0x106;              				// �������� CPU_CLK (0b0100000110)
  }
}

void spi_init()
{
	/* 
		 spi pins config: 
	   ssp2_txd - port d pin 5
		 ssp2_clk - port d pin 6
		 ssp2_rxd - port c pin 2
		 cs				- port d pin 7
	*/
	RST_CLK_PCLKcmd(RST_CLK_PCLK_PORTD, ENABLE);
	RST_CLK_PCLKcmd(RST_CLK_PCLK_PORTC, ENABLE);
		
	PORT_InitTypeDef port_conf;
	
	// txd, clk
	port_conf.PORT_Pin = ( PORT_Pin_5 | PORT_Pin_6 );
	port_conf.PORT_OE = PORT_OE_OUT;
	port_conf.PORT_FUNC = PORT_FUNC_ALTER;
	port_conf.PORT_MODE = PORT_MODE_DIGITAL;
	port_conf.PORT_PD = PORT_PD_DRIVER;
	port_conf.PORT_SPEED = PORT_SPEED_MAXFAST;
	PORT_Init(MDR_PORTD, &port_conf);
	// rxd
	port_conf.PORT_Pin = PORT_Pin_2;
	port_conf.PORT_OE = PORT_OE_IN;
	port_conf.PORT_FUNC = PORT_FUNC_OVERRID;	
	PORT_Init(MDR_PORTC, &port_conf);
	// chip select
	port_conf.PORT_Pin = PORT_Pin_7;
	port_conf.PORT_OE = PORT_OE_OUT;
	port_conf.PORT_FUNC = PORT_FUNC_PORT;	
	PORT_Init(MDR_PORTD, &port_conf);
	
	/* spi config */
	SSP_InitTypeDef spi_conf;
	const int rst_clk_ssp = RST_CLK_PCLK_SSP2;
	MDR_SSP_TypeDef     * spi = MDR_SSP2;
	
	RST_CLK_PCLKcmd(rst_clk_ssp, ENABLE);
	
	SSP_DeInit(spi);
	
	SSP_BRGInit(spi, SSP_HCLKdiv128);
	
	SSP_StructInit(&spi_conf);
	
	SSP_Init(spi, &spi_conf);
	
	SSP_Cmd(spi, ENABLE);
}

void uart_init(MDR_UART_TypeDef * UART, MDR_PORT_TypeDef * port, \
										uint16_t tx_pin, uint16_t rx_pin, \
										PORT_FUNC_TypeDef port_func, uint32_t baudrate)
{
  PORT_InitTypeDef GPIOInitStruct;

  UART_InitTypeDef UARTInitStruct;

  UART_BRGInit(UART, UART_HCLKdiv8);
	
	RST_CLK_PCLKcmd(RST_CLK_PCLK_PORTB, ENABLE);
	RST_CLK_PCLKcmd(RST_CLK_PCLK_UART1, ENABLE);

  // ����� ������������ ����� �����-������
  PORT_StructInit (&GPIOInitStruct);
  GPIOInitStruct.PORT_SPEED = PORT_SPEED_MAXFAST;
  GPIOInitStruct.PORT_MODE  = PORT_MODE_DIGITAL;

  // uart_rxd
	GPIOInitStruct.PORT_FUNC  = port_func;
  GPIOInitStruct.PORT_OE    = PORT_OE_IN;
  GPIOInitStruct.PORT_Pin   = rx_pin;
  PORT_Init (port, &GPIOInitStruct);

  // uart_txd 
  GPIOInitStruct.PORT_FUNC  = port_func;	
  GPIOInitStruct.PORT_OE    = PORT_OE_OUT;
  GPIOInitStruct.PORT_Pin   = tx_pin;
  PORT_Init (port, &GPIOInitStruct);

  // ������������ ������ UART
  UARTInitStruct.UART_BaudRate            = baudrate;                  			 // �������� �������� ������
  UARTInitStruct.UART_WordLength          = UART_WordLength8b;             // ���������� ����� ������ � ���������
  UARTInitStruct.UART_StopBits            = UART_StopBits1;                // ���������� STOP-�����
  UARTInitStruct.UART_Parity              = UART_Parity_No;                // �������� ��������
  UARTInitStruct.UART_FIFOMode            = UART_FIFO_OFF;                 // ���������/���������� ������
  UARTInitStruct.UART_HardwareFlowControl = UART_HardwareFlowControl_RXE   // ���������� �������� �� ��������� � ������� ������
                                          | UART_HardwareFlowControl_TXE;

  // ������������� ������ UART
  UART_Init (UART, &UARTInitStruct);

  // ����� ���������� ���������� (����� � �������� ������)
  UART_ITConfig (UART, UART_IT_RX | UART_IT_TX, ENABLE);

  // ���������� ������ ������ UART
  UART_Cmd (UART, ENABLE);
}

void NVIC_init()
{
	NVIC_SetPriority (UART1_IRQn, 1);
	NVIC_EnableIRQ(UART1_IRQn);
	
	NVIC_SetPriority (UART2_IRQn, 1);
	NVIC_EnableIRQ(UART2_IRQn);
	
	NVIC_SetPriority (Timer1_IRQn, 1);
	NVIC_EnableIRQ(Timer1_IRQn);
	
	NVIC_SetPriority (Timer2_IRQn, 1);
	NVIC_EnableIRQ(Timer2_IRQn);
}

void timer_init(MDR_TIMER_TypeDef * tmr)
{
	RST_CLK_PCLKcmd(RST_CLK_PCLK_TIMER1, ENABLE);
	RST_CLK_PCLKcmd(RST_CLK_PCLK_TIMER2, ENABLE);
	
	TIMER_BRGInit(tmr, TIMER_HCLKdiv128);
	
	TIMER_DeInit(tmr);
	
	tmr->CNT = 0xffff;
	tmr->CNTRL |= TIMER_CntDir_Dn;
	tmr->IE |= 0x1;									// enable interrupt when cnt = 0
}
