#include "crm100.h"

uint8_t get_checksum(const msg_t msg)
{
	return ~(msg.cmd_status + msg.data0 + msg.data1 + msg.data2 + msg.data3);
}

msg_t set_cmd_msg(const bool rrs, const bool cbit, const char rr)
{
	uint8_t cmd = 0;
	if(rrs) cmd |= INTERNAL_RATE_RANGE_MSK;
	if(cbit) cmd |= CBIT_MSK;
	cmd &= ~(RATE_RANGE_MSK);
  cmd |= ((rr << 3)	& RATE_RANGE_MSK);
			
	msg_t rv = { cmd, 0, 0, 0, 0, 0 };
	return rv;
}	

msg_t get_stt_msg(MDR_SSP_TypeDef* SSP, const msg_t cmd)
{
	msg_t rv;
	
	for(int i = 0xfff; i != 0; i--);
	
	SSP_SendData(SSP, cmd.cmd_status);
	SSP_SendData(SSP, cmd.data0);
	SSP_SendData(SSP, cmd.data1);
	SSP_SendData(SSP, cmd.data2);
	SSP_SendData(SSP, cmd.data3);
	SSP_SendData(SSP, get_checksum(cmd));
	
	for(int i = 0xfff; i != 0; i--);
	
	rv.cmd_status = static_cast<uint8_t>(SSP_ReceiveData(SSP));
	rv.data0 = static_cast<uint8_t>(SSP_ReceiveData(SSP));
	rv.data1 = static_cast<uint8_t>(SSP_ReceiveData(SSP));
	rv.data2 = static_cast<uint8_t>(SSP_ReceiveData(SSP));
	rv.data3 = static_cast<uint8_t>(SSP_ReceiveData(SSP));
	rv.checksum = static_cast<uint8_t>(SSP_ReceiveData(SSP));
	
	return rv;
}

uint16_t msg_t::rate()
{
	uint16_t rv = data0;
	return ((rv << 8) | data1);
}

uint16_t msg_t::temp_code()
{
	uint16_t rv = data2;
	return ((rv << 8) | data3 );
}
