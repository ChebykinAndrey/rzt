#include "com.h"

char * strchr(char * str, const char chr, int order)
{
	  if(order == 0) return NULL;
    if(order - 1 == 0) return strchr(str, chr);

    return strchr(strchr(str, chr) + 1, chr, --order);
}
