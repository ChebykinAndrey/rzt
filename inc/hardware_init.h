#include "MDR32Fx.h"
#include "MDR32F9Qx_port.h"
#include "MDR32F9Qx_ssp.h"
#include "MDR32F9Qx_uart.h"

void spi_init();
void Frequency_CLK_PLL(unsigned pll_on, unsigned int pll_mul);
void uart_init(MDR_UART_TypeDef * UART, MDR_PORT_TypeDef * port, \
										uint16_t tx_pin, uint16_t rx_pin, \
										PORT_FUNC_TypeDef port_func, uint32_t baudrate);
void NVIC_init();
void timer_init(MDR_TIMER_TypeDef * tmr);