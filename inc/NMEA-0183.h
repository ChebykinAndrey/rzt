#include "stddef.h"

template<size_t max_size>
struct NMEA-0183
{
	array<char, size> buffer;
	size_t size;
	void set_msg(const char * msg)
	{
		char check_sum = 0;
		
		buffer[0] = "$";
		
		int i = 1;
		while((i < strlen(msg)) && (i < size))
		{
			check_sum ^= msg;
			buffer[i] = *msg++;
		}
		buffer[i++] = "*";
		buffer[i] = NULL;
		
		size = i + 1;
	}
};
	

