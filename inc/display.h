#include "stdint.h"
#include "string.h"

#include "lcd.h"
#include "text.h"

template<int fw, int fh, int max_x, int max_y>
class display
{
private:
	int x, y;
	
public:
	static const int column = max_x/fw;
	static const int row = max_y/fh;

	display() : x(0), y(0) {}
	void putc(const char chr)
	{
		if(x == column) 
		{
			if(++y < row)
				x = 0;
			else
				--y;
		}
		putc(x++*fw, y*fh, chr);
	}
	void putc(const int x, const int y, const char ch) { LCD_PUTC(x, y, ch); }
	void puts(const char * str)
	{
		for(int i = 0; i < strlen(str); i++) putc(str[i]);
	}
	display& new_line(const char * str)
	{
		x = 0;
		if(++y < row)
		{
			*this << str;
		}
		else
			--y;
	}
	void cls() 
	{
		y = 0; x = 0;
		LCD_CLS();
	}
	display& operator<<(const char * str) 
	{
		char buf[column*row];
		snprintf(buf, column*row, "%s", str);
		puts(buf);
		return *this;
	}
	display& operator<<(const int number) 
	{
		char buf[column];
		sprintf(buf, "%d", number);
		*this << buf;
		return *this;
	}
	display& operator<<(const float number)
	{
		char buf[column];
		sprintf(buf, "%3.2f", number);
		*this << buf;
		return *this;
	}
};
