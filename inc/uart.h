#include "stddef.h"
#include "MDR32Fx.h"
#include "MDR32F9Qx_uart.h"

template<class D, size_t rx_buf_size>
class UART
{
private:
	char * tx_src;
	char * rx_dst;
	size_t data_num, index;
	MDR_UART_TypeDef * uart;
	char rx_buffer[rx_buf_size];

public:
	UART(MDR_UART_TypeDef * uart) : data_num(0), index(0), uart(uart) {}

int last_tr_size() { return static_cast<int>(index); }
int	get_rx_data(char * dst)
{ 
	int rv = index;
	for(int i = 0; i < index; i++)
	{
		dst[i] = rx_buffer[i];
	}
	finish_transaction();
	return rv;
}
		
void send(char * src, size_t length)
{
	wait();
	data_num = length; index = 0;
	tx_src = src;
	//for(auto i = 0; i < length && i < tx_buffer_size; i++) tx_buffer[i] = data[i];
	enable_tx();
	putc(*tx_src); //to start IRQ ticks
}

void receive(char * dst)
{
	wait();
	index = 0;
	rx_dst = dst;
	enable_rx();
	set_timeout();
}

void cnt_rcv()
{
	wait();
	enable_rx();
	//set_timeout();
}

void putc(const char ch)
{
	UART_SendData(uart, ch);
}

const char getc()
{
	return UART_ReceiveData(uart);
}	
		
void set_timeout() { static_cast<D*>(this)->set_timeout(); }
	
void wait()
{
	if(data_num || index)
	{
		// wait transaction end
		do { __NOP(); } while (data_num);
	}
}
		
	void enable_tx() { UART_ITConfig (uart, UART_IT_TX, ENABLE); }
	void enable_rx() { UART_ITConfig (uart, UART_IT_RX, ENABLE); }
	void disable_tx() { UART_ITConfig (uart, UART_IT_TX, DISABLE); }
	void disable_rx() { UART_ITConfig (uart, UART_IT_RX, DISABLE); }

void RXIRQHandler()
{
	if(index < rx_buf_size)
		rx_buffer[index++] = getc();
	else
		char c = getc();
	set_timeout();
}

void TXIRQHandler()
{
	putc(tx_src[index++]);
	if(index == data_num)
	{
		disable_tx();
		finish_transaction();
	}
}

void IRQHandler()
{
	if(UART_GetITStatusMasked (uart, UART_IT_RX) == SET)
	{
		RXIRQHandler();
	}
	else if(UART_GetITStatusMasked(uart, UART_IT_TX) == SET)
	{
		TXIRQHandler();
	}		
}

size_t finish_transaction()
{
	int rv = index;
//	disable_tx();
//	disable_rx();
	index = 0; data_num = 0;
	return rv;
}

};
