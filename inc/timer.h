#include "com.h"
#include "MDR32F9Qx_timer.h"

template<class D, uint tmr_freq>
class timer
{
private:
	MDR_TIMER_TypeDef * tmr;
public:
	void clear_interrupts() { tmr->STATUS = 0x0; }
public:
	timer(MDR_TIMER_TypeDef * tmr) : tmr(tmr) { clear_interrupts(); }
	void start() { tmr->CNTRL |= 0x00000001; }
	void stop() { tmr->CNTRL &= ~0x00000001; }
	void timeout_us(uint timeout)
	{
		stop();
		tmr->CNT = tmr_freq/1e6f * timeout;
		start();
	}
	void IRQHandler()
	{ 
		stop();
		clear_interrupts();
		static_cast<D*>(this)->callback();
	}
	
};
