#include "stdint.h"
#include "stddef.h"

template<class D, class data_t>
class spi
{
public:
	void send(uint8_t * data, size_t len);
	void read(uint8_t * dst, size_t len);
	inline void irq_handler() { static_cast<D*>(this)->irq_hdr(); };
};