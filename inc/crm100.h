#include <stdint.h>
#include "MDR32F9Qx_ssp.h"

#define INTERNAL_RATE_RANGE_MSK 0x20
#define RATE_RANGE_MSK 0x18
#define CBIT_MSK 0x6

struct msg_t {
	uint8_t cmd_status;
	uint8_t data0;
	uint8_t data1;
	uint8_t data2;
	uint8_t data3;
	uint8_t checksum;
	uint16_t rate();
	uint16_t temp_code();
};
uint8_t get_checksum(const msg_t msg);
msg_t set_cmd_msg(const bool rrs, const bool cbit, const char rr);
msg_t get_stt_msg(MDR_SSP_TypeDef* SSPx, const msg_t cmd);
